#!/bin/bash
while :
do
echo "1. List all screening"
echo "2. Choose screening"
echo "3. Make reservation (1x Adult)"
echo "4. Make reservation with voucher (1x Adult)"
echo -n "Type 1, 2, 3 or 4:"
read -n 1 a
printf "\n"
case $a in
1* )     curl  -H "accept: */*" -X GET "http://localhost:9010/screenings?from=2019-07-09T08%3A15%3A53&to=2019-09-09T08%3A15%3A53" | jq
         ;;

2* )     echo "Type screening identifier"
         read screening_id
         curl  -H "accept: */*" -X GET "http://localhost:9010/screenings/$screening_id" | jq
         ;;
3* )     echo "Type screening identifier"
         read screening_id
         echo "Type seat identifier"
         read seat_id
         curl -X POST "http://localhost:9010/reservation" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"customer\": { \"name\": \"Żaneta\", \"surname\": \"Dębińska-Łątka\" }, \"rebateCode\": \"\", \"screeningId\": { \"id\": \"$screening_id\" }, \"seatIds\": [ { \"id\": \"$seat_id\" } ], \"tickets\": { \"ADULT\": { \"quantity\": 1 } }}" | jq
         ;;
4* )     echo "Type screening identifier"
         read screening_id
         echo "Type seat identifier"
         read seat_id
         curl -X POST "http://localhost:9010/reservation" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"customer\": { \"name\": \"Żaneta\", \"surname\": \"Dębińska-Łątka\" }, \"rebateCode\": \"PROMO\", \"screeningId\": { \"id\": \"$screening_id\" }, \"seatIds\": [ { \"id\": \"$seat_id\" } ], \"tickets\": { \"ADULT\": { \"quantity\": 1 } }}" | jq
         ;;
* )     echo "Try again.";;
esac
done
