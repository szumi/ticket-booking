## Ticket booking app - TouK recruitment task

### Extra assumptions
1. In addition to provide seats and name of the person doing reservation, type of ticket and quantity are required (It's needed for determine amount to pay).
2. Number of reserved seats and sum of the tickets must be the same.
3. Number of seats in the room must be the same for all rows.
4. Outermost seats can be free, even if seat next to it is reserved.
5. Reservation expire time is set to 15 minutes before selected screening.
6. Ticket price policy for weekends is based on movie screening time.
7. Voucher rebate is applied to the end amount.

### Running

* ##### Requirements
    * `Maven` for building the jar file 
    * `Java 9+` (source version is 9)

* ##### Build & run application 
    Start `run.sh` script.

    Application will start at `localhost:9010`

#### Use case test script
* ##### Requirements
    * `Curl`
    * `jq`
 * ##### Running
    Start `use_case_test.sh` and follow instruction.


#### Api Documentation
`http://localhost:9010/swagger-ui.html`

