package pl.touk.cinema

import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import pl.touk.cinema.reservation.domain.ReservationSampleData
import pl.touk.cinema.reservation.domain.ReservationTestConfiguration
import pl.touk.cinema.screening.domain.ScreeningSampleData
import pl.touk.cinema.screening.domain.ScreeningService
import spock.lang.Specification

import java.time.format.DateTimeFormatter

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles(["test"])
@SpringBootTest(classes = [ApplicationRunner, ReservationTestConfiguration])
class IntegrationSpecificationTest extends Specification implements ScreeningSampleData, ReservationSampleData {

  @Autowired
  private WebApplicationContext webApplicationContext

  @Autowired
  private ScreeningService screeningService

  MockMvc mockMvc

  @Before
  void setupMvc() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
        .build();
  }

  def "Ticket booking positive base scenario"() {
    given: 'Fast & Furious at 2019-08-02 13:30:00 and Star Wars at 2019-08-02 14:00:00'
      def savedFastAndFurious = screeningService.add(fastAndFurious)
      screeningService.add(startWars)

    when: 'I GET /screenings?from=2019-08-02T13:30:00&to=2019-08-02T13:59:59'
      ResultActions resultActions = mockMvc.perform(get("/screenings")
          .param("from", "2019-08-02T13:30:00")
          .param("to", "2019-08-02T13:59:59")
      )
    then: 'I see Fast & Furious screening info'
      resultActions.andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(content().json("""
            [
                {
                    "id": {
                        "id": "$savedFastAndFurious.id.id"
                    },
                    "movie": {
                        "title": "$fastAndFurious.movie.title"
                    },
                    "screeningTime": "${fastAndFurious.screeningTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}"
                }
            ]
            """))

    when: 'I GET /screenings with Fast & Furious'
      resultActions = mockMvc.perform(get("/screenings/{id}", savedFastAndFurious.id.id))
    then: 'I see Fast & Furious screening details'
      def screeningDetails = screeningService.get(savedFastAndFurious.getId())
      resultActions.andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(content().json("""
                {
                  "id": {
                    "id": "$savedFastAndFurious.id.id"
                  },
                 "availableSeats":
                   ${screeningDetails.seats.collect { seat -> """
                    {
                    "id" : {
                      "id": $seat.id.id
                    },
                    "rowNumber": $seat.rowNumber,
                    "seatNumber": $seat.seatNumber
                    }"""
                   }},
                  "movie": {
                    "title": "$fastAndFurious.movie.title"
                  },
                  "room": {
                    "name": "$fastAndFurious.room.name",
                    "rowSeatsNumber": $fastAndFurious.room.rowSeatsNumber,
                    "rowsNumber": $fastAndFurious.room.rowsNumber
                  },
                  screeningTime: "${fastAndFurious.screeningTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}"
                }
                """))

    when: 'I POST /reservation for Fast & Furious screening'

      resultActions = mockMvc.perform(post("/reservation")
          .contentType(MediaType.APPLICATION_JSON)
          .content("""
                  {
                    "customer": {
                      "name": "Żaneta",
                      "surname": "Dębińska-Łątka"
                    },
                    "rebateCode": "",
                    "screeningId": {
                      "id": "$savedFastAndFurious.id.id"
                    },
                    "seatIds": [
                      {
                        "id": "${screeningDetails.getSeats().get(0).id.id}"
                      }, {
                        "id": "${screeningDetails.getSeats().get(1).id.id}"
                      }, {
                        "id": "${screeningDetails.getSeats().get(2).id.id}"
                      }
                    ],
                    "tickets": {
                      "ADULT": {
                        "quantity": 1
                      },
                      "STUDENT": {
                        "quantity": 1
                      },
                      "CHILD": {
                        "quantity": 1
                      }
                    }
                  }
                  """)

      )

    then: 'I see amount to pay and reservation expiration time'
      resultActions
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(content().json("""
                  {
                    "amount": {
                      "amount": 55.50,
                      "currency": "PLN"
                    },
                    "expirationTime": "${fastAndFurious.screeningTime.minusMinutes(15).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}"
                  }
                """))
  }

  def "Ticket booking positive scenario with rebate and weekend"() {
    given: 'Fast & Furious at 2019-08-02 13:30:00 and Star Wars at 2019-08-02 14:00:00'
      screeningService.add(fastAndFurious)
      def savedStarWars = screeningService.add(startWars)

    when: 'I GET /screenings?from=2019-08-02T14:00:00&to=2019-08-02T15:00:00'
      ResultActions resultActions = mockMvc.perform(get("/screenings")
          .param("from", "2019-08-02T14:00:00")
          .param("to", "2019-08-02T15:00:00")
      )
    then: 'I see Start Wars screening info'
      resultActions.andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(content().json("""
              [
                  {
                      "id": {
                          "id": "$savedStarWars.id.id"
                      },
                      "movie": {
                          "title": "$savedStarWars.movie.title"
                      },
                      "screeningTime": "${savedStarWars.screeningTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}"
                  }
              ]
              """))

    when: 'I GET /screenings with Start Wars'
      resultActions = mockMvc.perform(get("/screenings/{id}", savedStarWars.id.id))
      then: 'I see Start Wars screening details'
    def screeningDetails = screeningService.get(savedStarWars.getId())
      resultActions.andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(content().json("""
                  {
                    "id": {
                      "id": "$savedStarWars.id.id"
                    },
                   "availableSeats":
                     ${screeningDetails.seats.collect { seat -> """
                      {
                      "id" : {
                        "id": $seat.id.id
                      },
                      "rowNumber": $seat.rowNumber,
                      "seatNumber": $seat.seatNumber
                      }"""
          }},
                    "movie": {
                      "title": "$savedStarWars.movie.title"
                    },
                    "room": {
                      "name": "$savedStarWars.room.name",
                      "rowSeatsNumber": $savedStarWars.room.rowSeatsNumber,
                      "rowsNumber": $savedStarWars.room.rowsNumber
                    },
                    screeningTime: "${savedStarWars.screeningTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}"
                  }
                  """))

    when: 'I POST /reservation for Start Wars screening'

    resultActions = mockMvc.perform(post("/reservation")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
                  {
                    "customer": {
                      "name": "Żaneta",
                      "surname": "Dębińska-Łątka"
                    },
                    "rebateCode": "PROMO",
                    "screeningId": {
                      "id": "$savedStarWars.id.id"
                    },
                    "seatIds": [
                      {
                        "id": "${screeningDetails.getSeats().get(0).id.id}"
                      }, {
                        "id": "${screeningDetails.getSeats().get(1).id.id}"
                      }, {
                        "id": "${screeningDetails.getSeats().get(2).id.id}"
                      }
                    ],
                    "tickets": {
                      "ADULT": {
                        "quantity": 1
                      },
                      "STUDENT": {
                        "quantity": 1
                      },
                      "CHILD": {
                        "quantity": 1
                      }
                    }
                  }
                  """)

    )

    then: 'I see amount to pay and reservation expiration time'
    resultActions
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(content().json("""
                  {
                    "amount": {
                      "amount": 33.75,
                      "currency": "PLN"
                    },
                    "expirationTime": "${startWars.screeningTime.minusMinutes(15).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)}"
                  }
                """))
  }

  //TODO add tests for negative scenarios
}
