package pl.touk.cinema.reservation.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.time.Clock;

import static java.time.LocalDateTime.of;
import static java.time.ZoneOffset.UTC;

@Configuration
public class ReservationTestConfiguration {

  @Bean
  @Primary
  Clock clock() {
    return Clock.fixed(of(2018, 8, 1, 19, 30).toInstant(UTC), UTC);
  }
}
