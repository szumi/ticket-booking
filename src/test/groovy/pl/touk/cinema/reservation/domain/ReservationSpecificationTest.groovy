package pl.touk.cinema.reservation.domain

import pl.touk.cinema.reservation.dto.CreateReservationDto
import pl.touk.cinema.reservation.dto.QuantityDto
import pl.touk.cinema.reservation.exception.ReservationValidationException
import pl.touk.cinema.screening.domain.ScreeningConfiguration
import pl.touk.cinema.screening.domain.ScreeningSampleData
import pl.touk.cinema.screening.domain.ScreeningService
import pl.touk.cinema.screening.dto.AmountDto
import pl.touk.cinema.screening.dto.CustomerDto
import spock.lang.Specification

import static pl.touk.cinema.reservation.dto.TicketTypeDto.ADULT
import static pl.touk.cinema.reservation.dto.TicketTypeDto.CHILD

class ReservationSpecificationTest extends Specification implements ReservationSampleData, ScreeningSampleData {

  ScreeningService screeningService = new ScreeningConfiguration().screeningService();

  ReservationService reservationService = new ReservationConfiguration().reservationService(defaultClock, screeningService);

  def cleanup() {
    screeningService = new ScreeningConfiguration().screeningService();
    reservationService = new ReservationConfiguration().reservationService(defaultClock, screeningService);
  }

  def 'Should create screening reservation'() {
    given: 'FilmA screening'
      def filmAScreening = screeningService.add(filmC)
      def chosenScreening = screeningService.choose(filmAScreening.id)
    when: 'I make reservation for FilmA'
      def createReservationPayload = CreateReservationDto.builder()
        .screeningId(filmAScreening.id)
        .customer(new CustomerDto("Żaneta", "Dębińska-Łątka"))
        .tickets(Map.of(ADULT, new QuantityDto(1), CHILD, new QuantityDto(2)))
        .seatIds(Set.of(chosenScreening.availableSeats.get(0).getId(), chosenScreening.availableSeats.get(5).getId(), chosenScreening.availableSeats.get(6).getId()))
        .build();

      def createdReservation = reservationService.createReservation(createReservationPayload)
    then: 'I see amount to pay and reservation expiration time'
      createdReservation.amount.currency.symbol == currency.symbol
      createdReservation.amount.amount == new AmountDto(new BigDecimal(50), currency).amount
      createdReservation.expirationTime == chosenScreening.screeningTime.minusMinutes(15)
  }

  def 'Should not create reservation when customer name format is invalid'() {
    given: 'FilmA screening'
      def filmAScreening = screeningService.add(filmC)
      def chosenScreening = screeningService.choose(filmAScreening.id)
    when: 'I make reservation for FilmA and customer name and surname is too short'
      def createReservationPayload = CreateReservationDto.builder()
        .screeningId(filmAScreening.id)
        .customer(new CustomerDto("An", "Ka"))
        .tickets(Map.of(ADULT, new QuantityDto(1), CHILD, new QuantityDto(2)))
        .seatIds(Set.of(chosenScreening.availableSeats.get(0).getId(), chosenScreening.availableSeats.get(5).getId(), chosenScreening.availableSeats.get(6).getId()))
        .build();

      reservationService.createReservation(createReservationPayload)
    then: 'Error is thrown'
      def ex = thrown(ReservationValidationException)
      ex.getErrors().contains("Invalid format of customer name")
  }

  def 'Should create screening reservation with voucher rebate'() {
    given: 'FilmA screening'
      def filmAScreening = screeningService.add(filmC)
      def chosenScreening = screeningService.choose(filmAScreening.id)
    when: 'I make reservation for FilmA'
      def createReservationPayload = CreateReservationDto.builder()
        .screeningId(filmAScreening.id)
        .customer(new CustomerDto("Żaneta", "Dębińska-Łątka"))
        .tickets(Map.of(ADULT, new QuantityDto(1), CHILD, new QuantityDto(2)))
        .seatIds(Set.of(chosenScreening.availableSeats.get(0).getId(), chosenScreening.availableSeats.get(5).getId(), chosenScreening.availableSeats.get(6).getId()))
        .rebateCode("PROMO")
        .build();

      def createdReservation = reservationService.createReservation(createReservationPayload)
    then: 'I see amount to pay with rebate and reservation expiration time'
      createdReservation.amount.currency.symbol == currency.symbol
      createdReservation.amount.amount == new AmountDto(new BigDecimal(25), currency).amount
      createdReservation.expirationTime == chosenScreening.screeningTime.minusMinutes(15)
  }

  def 'Should create screening reservation with weekend rebate'() {
    //TODO change the way of setting clock
    reservationService = new ReservationConfiguration().reservationService(weekendClock, screeningService);

    given: 'FilmC screening'
      def filmCScreening = screeningService.add(filmD)
      def chosenScreening = screeningService.choose(filmCScreening.id)
    when: 'I make reservation for FilmA'
      def createReservationPayload = CreateReservationDto.builder()
        .screeningId(filmCScreening.id)
        .customer(new CustomerDto("Żaneta", "Dębińska-Łątka"))
        .tickets(Map.of(ADULT, new QuantityDto(1), CHILD, new QuantityDto(2)))
        .seatIds(Set.of(chosenScreening.availableSeats.get(0).getId(), chosenScreening.availableSeats.get(1).getId(), chosenScreening.availableSeats.get(2).getId()))
        .build();

      def createdReservation = reservationService.createReservation(createReservationPayload)
    then: 'I see amount to pay with rebate and reservation expiration time'
      createdReservation.amount.currency.symbol == currency.symbol
      createdReservation.amount.amount == new AmountDto(new BigDecimal(62), currency).amount
      createdReservation.expirationTime == chosenScreening.screeningTime.minusMinutes(15)
  }
}
