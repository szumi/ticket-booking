package pl.touk.cinema.reservation.domain

import groovy.transform.CompileStatic

import java.time.Clock

import static java.time.LocalDateTime.of
import static java.time.ZoneOffset.UTC

@CompileStatic
trait ReservationSampleData {

  Clock defaultClock = Clock.fixed(of(2019, 8, 7, 19, 30).toInstant(UTC), UTC)
  Clock weekendClock = Clock.fixed(of(2019, 8, 16, 19, 30).toInstant(UTC), UTC)
  Locale locale = new Locale("pl", "PL");
  Currency currency = Currency.getInstance(locale)

}
