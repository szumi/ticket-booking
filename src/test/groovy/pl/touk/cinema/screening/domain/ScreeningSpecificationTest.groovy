package pl.touk.cinema.screening.domain

import pl.touk.cinema.screening.dto.ChooseScreeningDto
import pl.touk.cinema.screening.dto.ListScreeningFilterDto
import pl.touk.cinema.screening.dto.ScreeningIdDto
import pl.touk.cinema.screening.exception.InvalidDateRangeException
import pl.touk.cinema.screening.exception.ScreeningNotFoundException
import pl.touk.cinema.screening.exception.ScreeningSeatsValidationException
import spock.lang.Specification

import static java.time.LocalDateTime.of

class ScreeningSpecificationTest extends Specification implements ScreeningSampleData {

  ScreeningService screeningService = new ScreeningConfiguration().screeningService();

  def cleanup() {
    screeningService = new ScreeningConfiguration().screeningService();
  }

  def 'Should find screenings in given time range'() {

    given: 'FilmA at 2019-08-07 19:30:00 and FilmB at 2019-08-07 19:45:00'
      screeningService.add(filmA)
      screeningService.add(filmB)

    when: 'I get all screenings between 2019-08-07 19:00:00-20:00:00'
      def screenings = screeningService.getScreeningsBy(new ListScreeningFilterDto(of(2019, 8, 8, 19, 0), of(2019, 8, 8, 20, 0)))
    then: 'I have FilmA and FilmB screening info'
      screenings.size() == 2

    when: 'I get all screenings between 2019-08-07 19:30:00-19:45:00'
      screenings = screeningService.getScreeningsBy(new ListScreeningFilterDto(of(2019, 8, 8, 19, 30), of(2019, 8, 8, 19, 45)))
    then: 'I have FilmA and FilmB screening info'
      screenings.size() == 2

    when: 'I get all screenings between 2019-08-07 19:00:00-19:29:59'
      screenings = screeningService.getScreeningsBy(new ListScreeningFilterDto(of(2019, 8, 8, 19, 0), of(2019, 8, 8, 19, 29, 59)))
    then: 'I have empty screening info'
      screenings.size() == 0

    when: 'I get all screenings between 2019-08-07 19:45:01-20:00:00'
      screenings = screeningService.getScreeningsBy(new ListScreeningFilterDto(of(2019, 8, 8, 19, 45, 1), of(2019, 8, 8, 20,0)))
    then: 'I have empty screening info'
      screenings.size() == 0

    when: 'I get all screenings between 2019-08-07 19:30:00-19:40:00'
      screenings = screeningService.getScreeningsBy(new ListScreeningFilterDto(of(2019, 8, 8, 19, 30), of(2019, 8, 8, 19, 40)))
    then: 'I have FilmA screening info'
      screenings.size() == 1
      screenings.get(0).id != null
      screenings.get(0).movie == filmA.movie
      screenings.get(0).screeningTime == filmA.screeningTime

    when: 'I get all screenings between 2019-08-07 19:40:00-19:45:00'
      screenings = screeningService.getScreeningsBy(new ListScreeningFilterDto(of(2019, 8, 8, 19, 40), of(2019, 8, 8, 19, 45)))
    then: 'I have FilmB screening info'
      screenings.size() == 1
      screenings.get(0).id != null
      screenings.get(0).movie == filmB.movie
      screenings.get(0).screeningTime == filmB.screeningTime

    when: 'I pass invalid date range'
      screeningService.getScreeningsBy(new ListScreeningFilterDto(of(2019, 9, 8, 19, 40), of(2018, 8, 8, 19, 45)))
    then: 'Error is thrown'
      thrown(InvalidDateRangeException)
  }

  def 'Should get screening details'() {
    given: 'FilmA screening'
      def filmAScreening = screeningService.add(filmA)

    when: 'I choose FilmA'
      ChooseScreeningDto chosenScreening = screeningService.choose(filmAScreening.id)
    then: 'I have FilmA screening details'
      chosenScreening.id == filmAScreening.id
      chosenScreening.movie == filmAScreening.movie
      chosenScreening.screeningTime == filmAScreening.screeningTime
      chosenScreening.room == filmAScreening.room
      chosenScreening.availableSeats.size() == filmAScreening.room.rowSeatsNumber * filmAScreening.room.rowsNumber

    when: 'I choose not existing screening'
      screeningService.choose(new ScreeningIdDto(UUID.randomUUID().toString()))
    then: 'Error is thrown'
      thrown(ScreeningNotFoundException)
  }

  def 'Should reserve screening seats'() {
    given: 'FilmA screening'
      def filmAScreening = screeningService.add(filmA)
      def chosenScreening = screeningService.choose(filmAScreening.id)

    when: 'I reserve seats for FilmA screening'
      screeningService.reserveSeats(filmAScreening.id, Set.of(chosenScreening.availableSeats.get(0).getId(),
          chosenScreening.availableSeats.get(1).getId(),
          chosenScreening.availableSeats.get(2).getId())
      )

    then: 'Seats should be reserved'
      def screeningDetails = screeningService.get(filmAScreening.id)
      screeningDetails.getSeats()
        .stream()
        .filter({ seat -> !seat.isAvailable() })
        .count() == 3
      !screeningDetails.getSeats().get(0).isAvailable()
      !screeningDetails.getSeats().get(1).isAvailable()
      !screeningDetails.getSeats().get(2).isAvailable()
  }

  def 'Should not reserve seats - there cannot be a single place left over in a row between two already reserved places'() {
    given: 'FilmA screening'
      def filmAScreening = screeningService.add(filmA)
      def chosenScreening = screeningService.choose(filmAScreening.id)

    when: 'I reserve seats for FilmA with empty seat between two reserved seats'
      screeningService.reserveSeats(filmAScreening.id, Set.of(chosenScreening.availableSeats.get(0).getId(),
        chosenScreening.availableSeats.get(1).getId(),
        chosenScreening.availableSeats.get(3).getId())
    )

    then: 'Error is thrown'
      thrown(ScreeningSeatsValidationException)
  }
}
