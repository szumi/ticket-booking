package pl.touk.cinema.screening.domain

import groovy.transform.CompileStatic
import pl.touk.cinema.screening.dto.CreateScreeningDto
import pl.touk.cinema.screening.dto.MovieDto
import pl.touk.cinema.screening.dto.RoomDto

import static java.time.LocalDateTime.of

@CompileStatic
trait ScreeningSampleData {

  CreateScreeningDto filmA = CreateScreeningDto.builder()
      .movie(new MovieDto("FilmA"))
      .room(new RoomDto("A", 20, 5))
      .screeningTime(of(2019, 8, 8, 19, 30))
      .build();

  CreateScreeningDto filmB = CreateScreeningDto.builder()
      .movie(new MovieDto("FilmB"))
      .room(new RoomDto("B", 11, 10))
      .screeningTime(of(2019, 8, 8, 19, 45))
      .build();

  CreateScreeningDto filmC = CreateScreeningDto.builder()
      .movie(new MovieDto("FilmA"))
      .room(new RoomDto("A", 20, 5))
      .screeningTime(of(2019, 8, 8, 19, 30))
      .build();

  CreateScreeningDto filmD = CreateScreeningDto.builder()
      .movie(new MovieDto("FilmC"))
      .room(new RoomDto("C", 5, 5))
      .screeningTime(of(2019, 8, 17, 19, 45))
      .build();

  CreateScreeningDto fastAndFurious = CreateScreeningDto.builder()
      .movie(new MovieDto("Fast and Furious"))
      .room(new RoomDto("Room 1", 10, 6))
      .screeningTime(of(2019, 8, 2, 13, 30,0))
      .build()

  CreateScreeningDto startWars = CreateScreeningDto.builder()
      .movie(new MovieDto("Start Wars"))
      .room(new RoomDto("Room 2", 4, 5))
      .screeningTime(of(2019, 8, 2, 14, 0, 0))
      .build()
}
