package pl.touk.cinema.infrastructure.adapters.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.touk.cinema.reservation.exception.ReservationValidationException;
import pl.touk.cinema.reservation.exception.TicketAmountCalculationException;
import pl.touk.cinema.reservation.exception.TicketPriceCalculatorNotFound;
import pl.touk.cinema.screening.exception.InvalidDateRangeException;
import pl.touk.cinema.screening.exception.NotSupportedOperationException;
import pl.touk.cinema.screening.exception.ScreeningNotFoundException;
import pl.touk.cinema.screening.exception.ScreeningSeatsValidationException;
import pl.touk.cinema.screening.exception.SeatNotFoundException;

import java.util.List;

import static java.util.Collections.emptyList;

@RestControllerAdvice
class ExceptionHandlerAdvice {

  @Getter
  @AllArgsConstructor
  private static class ExceptionDetails {
    private String message;
    private List<String> errors;
  }

  @ExceptionHandler(value = {
    InvalidDateRangeException.class, ScreeningNotFoundException.class, SeatNotFoundException.class, IllegalArgumentException.class
  })
  ResponseEntity<ExceptionDetails> handleBadRequest(Exception ex) {
    return getResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(value = {ReservationValidationException.class})
  ResponseEntity<ExceptionDetails> handleBadRequest(ReservationValidationException ex) {
    return getResponse(ex.getMessage(), ex.getErrors(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(value = {ScreeningSeatsValidationException.class})
  ResponseEntity<ExceptionDetails> handleBadRequest(ScreeningSeatsValidationException ex) {
    return getResponse(ex.getMessage(), ex.getErrors(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(value = {NotSupportedOperationException.class, TicketPriceCalculatorNotFound.class})
  ResponseEntity<ExceptionDetails> handleNotImplementedException(Exception ex) {
    return getResponse(ex.getMessage(), HttpStatus.NOT_IMPLEMENTED);
  }

  @ExceptionHandler(value = {TicketAmountCalculationException.class})
  ResponseEntity<ExceptionDetails> handleInternalServerError(TicketAmountCalculationException ex) {
    return getResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
  }


  private ResponseEntity<ExceptionDetails> getResponse(String messageBody, HttpStatus status) {
    return getResponse(messageBody, emptyList(), status);
  }

  private ResponseEntity<ExceptionDetails> getResponse(String messageBody, List<String> errors, HttpStatus status) {
    ExceptionDetails body = new ExceptionDetails(messageBody, errors);
    return new ResponseEntity<>(body, status);
  }
}
