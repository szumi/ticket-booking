package pl.touk.cinema.reservation.exception;

public class TicketAmountCalculationException extends RuntimeException {

  public TicketAmountCalculationException(String message) {
    super(message);
  }
}
