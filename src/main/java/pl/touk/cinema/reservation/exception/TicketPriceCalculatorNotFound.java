package pl.touk.cinema.reservation.exception;

public class TicketPriceCalculatorNotFound extends RuntimeException {

  public TicketPriceCalculatorNotFound(String ticketType) {
    super("Ticket price calculator not found for ticket: " + ticketType);
  }
}
