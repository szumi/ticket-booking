package pl.touk.cinema.reservation.exception;

import lombok.Getter;

import java.util.List;

@Getter
public class ReservationValidationException extends RuntimeException {

  private List<String> errors;

  public ReservationValidationException(List<String> errors) {
    this.errors = errors;
  }
}
