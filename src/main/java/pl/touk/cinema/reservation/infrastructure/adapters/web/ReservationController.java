package pl.touk.cinema.reservation.infrastructure.adapters.web;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.touk.cinema.reservation.domain.ReservationService;
import pl.touk.cinema.reservation.dto.CreateReservationDto;
import pl.touk.cinema.reservation.dto.ReservationDto;

import javax.validation.Valid;

@RestController
@RequestMapping("/reservation")
@RequiredArgsConstructor
class ReservationController {

  private final ReservationService reservationService;

  @PostMapping
  ReservationDto create(@Valid @RequestBody CreateReservationDto payload) {
    return reservationService.createReservation(payload);
  }
}
