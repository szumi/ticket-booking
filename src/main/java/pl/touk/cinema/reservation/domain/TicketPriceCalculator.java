package pl.touk.cinema.reservation.domain;

import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.Currency;

interface TicketPriceCalculator {

  Amount calculate(Quantity quantity);

  boolean isApplicable(TicketType ticketType);

  @RequiredArgsConstructor
  class AdultTicketPriceCalculator implements TicketPriceCalculator {

    private final Currency currency;

    @Override
    public Amount calculate(Quantity quantity) {
      return new Amount(new BigDecimal(25).multiply(new BigDecimal(quantity.getQuantity())), currency);
    }

    @Override
    public boolean isApplicable(TicketType ticketType) {
      return TicketType.ADULT.equals(ticketType);
    }
  }

  @RequiredArgsConstructor
  class StudentTicketPriceCalculator implements TicketPriceCalculator {

    private final Currency currency;

    @Override
    public Amount calculate(Quantity quantity) {
      return new Amount(new BigDecimal(18).multiply(new BigDecimal(quantity.getQuantity())), currency);
    }

    @Override
    public boolean isApplicable(TicketType ticketType) {
      return TicketType.STUDENT.equals(ticketType);
    }
  }

  @RequiredArgsConstructor
  class ChildTicketPriceCalculator implements TicketPriceCalculator {

    private final Currency currency;

    @Override
    public Amount calculate(Quantity quantity) {
      return new Amount(new BigDecimal(12.5).multiply(new BigDecimal(quantity.getQuantity())), currency);
    }

    @Override
    public boolean isApplicable(TicketType ticketType) {
      return TicketType.CHILD.equals(ticketType);
    }
  }
}
