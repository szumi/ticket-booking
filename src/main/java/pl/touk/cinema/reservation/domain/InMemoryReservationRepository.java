package pl.touk.cinema.reservation.domain;

import java.util.concurrent.ConcurrentHashMap;

class InMemoryReservationRepository implements ReservationRepository {

  private final ConcurrentHashMap<ReservationId, Reservation> reservationMap = new ConcurrentHashMap<>();

  @Override
  public Reservation save(Reservation reservation) {
    reservationMap.put(reservation.getId(), reservation);
    return reservationMap.get(reservation.getId());
  }
}
