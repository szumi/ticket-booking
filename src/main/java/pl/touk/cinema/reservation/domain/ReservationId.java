package pl.touk.cinema.reservation.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import pl.touk.cinema.reservation.dto.ReservationIdDto;

import java.util.UUID;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
class ReservationId {

  private final UUID id = UUID.randomUUID();

  ReservationIdDto dto() {
    return new ReservationIdDto(id.toString());
  }
}
