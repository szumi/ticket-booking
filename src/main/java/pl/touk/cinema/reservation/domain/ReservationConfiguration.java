package pl.touk.cinema.reservation.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import pl.touk.cinema.screening.domain.ScreeningService;

import java.time.Clock;
import java.util.Currency;
import java.util.Locale;
import java.util.Set;

@Configuration
class ReservationConfiguration {

  @Bean
  public ReservationService reservationService(Clock clock, ScreeningService screeningService) {
    return new ReservationService(new ReservationCreator(clock, ticketPriceCalculatorFactory(), ticketRebatePolicyFactory(),
      screeningService), screeningService, reservationRepository(), new ReservationValidator());
  }

  @Bean
  public ReservationRepository reservationRepository() {
    return new InMemoryReservationRepository();
  }

  @Bean
  public Clock clock() {
    return Clock.systemDefaultZone();
  }

  private TicketPriceCalculatorFactory ticketPriceCalculatorFactory() {
    return new TicketPriceCalculatorFactory(Set.of(
      new TicketPriceCalculator.AdultTicketPriceCalculator(currency()),
      new TicketPriceCalculator.StudentTicketPriceCalculator(currency()),
      new TicketPriceCalculator.ChildTicketPriceCalculator(currency())
    ));
  }

  private TicketRebatePolicyFactory ticketRebatePolicyFactory() {
    return new TicketRebatePolicyFactory(Set.of(
      new TicketRebatePolicy.VoucherRebatePolicy(currency()),
      new TicketRebatePolicy.WeekendRebatePolicy(currency())
    ));
  }

  private Currency currency() {
    Locale locale = new Locale("pl", "PL");
    return Currency.getInstance(locale);
  }
}
