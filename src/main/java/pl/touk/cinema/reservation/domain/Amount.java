package pl.touk.cinema.reservation.domain;

import lombok.Getter;
import pl.touk.cinema.screening.dto.AmountDto;
import pl.touk.cinema.screening.exception.NotSupportedOperationException;

import java.math.BigDecimal;
import java.util.Currency;

import static java.math.RoundingMode.HALF_EVEN;

@Getter
class Amount {

  private BigDecimal amount;
  private Currency currency;

  Amount(BigDecimal that, Currency currency) {
    this.amount = that.setScale(currency.getDefaultFractionDigits(), HALF_EVEN);
    this.currency = currency;
  }

  Amount add(Amount amount) {
    if (!currency.getCurrencyCode().equals(amount.getCurrency().getCurrencyCode())) {
      throw new NotSupportedOperationException("Operation on various currencies are not supported.");
    }

    return new Amount(this.amount.add(amount.amount), currency);
  }

  AmountDto dto() {
    return new AmountDto(amount, currency);
  }
}
