package pl.touk.cinema.reservation.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
class TicketRebateFactor {

  private String rebateCode;
  private LocalDateTime screeningTime;
}
