package pl.touk.cinema.reservation.domain;

import lombok.RequiredArgsConstructor;
import pl.touk.cinema.reservation.dto.CreateReservationDto;
import pl.touk.cinema.reservation.exception.TicketAmountCalculationException;
import pl.touk.cinema.screening.domain.ScreeningService;
import pl.touk.cinema.screening.dto.DetailsScreeningDto;
import pl.touk.cinema.screening.dto.SeatIdDto;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toMap;

@RequiredArgsConstructor
class ReservationCreator {

  private final Clock clock;
  private final TicketPriceCalculatorFactory ticketPriceCalculatorFactory;
  private final TicketRebatePolicyFactory ticketRebatePolicyFactory;
  private final ScreeningService screeningService;

  public Reservation from(CreateReservationDto dto) {

    DetailsScreeningDto screening = screeningService.get(dto.getScreeningId());

    // Converts ticket info into domain representation
    Map<TicketType, Quantity> tickets = dto.getTickets()
        .entrySet()
        .stream()
        .collect(toMap(entry -> TicketType.valueOf(entry.getKey().name()), value -> new Quantity(value.getValue().getQuantity())));

    //Calculate reservation amount
    Amount totalAmount = tickets.entrySet()
        .stream()
        .map(ticketEntry -> {
          // Calculate base price
          TicketPriceCalculator calculator = ticketPriceCalculatorFactory.getPriceCalculator(ticketEntry.getKey());
          Amount baseAmount = calculator.calculate(ticketEntry.getValue());

          // Apply rebates
          List<TicketRebatePolicy> rebatePolicies = ticketRebatePolicyFactory
              .getRebatePolicies(new TicketRebateFactor(dto.getRebateCode(), screening.getScreeningTime()));

          Amount withRebateAmount = new Amount(baseAmount.getAmount(), baseAmount.getCurrency());
          for (TicketRebatePolicy rebatePolicy : rebatePolicies) {
            withRebateAmount = rebatePolicy.apply(withRebateAmount, ticketEntry.getValue());
          }

          return withRebateAmount;
        })
        .reduce(Amount::add)
        .orElseThrow(() -> new TicketAmountCalculationException("Cannot calculate ticket amount"));

    // Converts seats info into domain representation
    Set<SeatId> seats = dto.getSeatIds()
        .stream()
        .map(SeatIdDto::getId)
        .map(UUID::fromString)
        .map(SeatId::new)
        .collect(Collectors.toSet());

    // Determine expiration time
    LocalDateTime expirationTime = screening.getScreeningTime().minusMinutes(15);

    return new Reservation(new ReservationId(),
        new ScreeningId(UUID.fromString(dto.getScreeningId().getId())),
        new Customer(dto.getCustomer().getName(), dto.getCustomer().getSurname()),
        seats,
        tickets,
        now(clock),
        expirationTime,
        totalAmount);
  }
}
