package pl.touk.cinema.reservation.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import pl.touk.cinema.reservation.dto.ReservationDto;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
class Reservation {

  private ReservationId id;
  private ScreeningId screening;
  private Customer customer;
  private Set<SeatId> seats;
  private Map<TicketType, Quantity> tickets;
  private LocalDateTime createTime;
  private LocalDateTime expirationTime;
  private Amount amount;

  Integer getTotalTickets() {
    return getTickets().values()
        .stream()
        .mapToInt(Quantity::getQuantity)
        .sum();
  }

  public ReservationDto dto() {
    return ReservationDto.builder()
        .id(id.dto())
        .amount(amount.dto())
        .expirationTime(expirationTime)
        .build();
  }
}
