package pl.touk.cinema.reservation.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
class Customer {

  private String name;
  private String surname;

  public String getFullName() {
    return String.join(" ", name, surname);
  }
}
