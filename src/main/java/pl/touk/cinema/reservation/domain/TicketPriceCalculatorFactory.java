package pl.touk.cinema.reservation.domain;

import lombok.AllArgsConstructor;
import pl.touk.cinema.reservation.exception.TicketPriceCalculatorNotFound;

import java.util.Set;

@AllArgsConstructor
class TicketPriceCalculatorFactory {

  private final Set<TicketPriceCalculator> calculators;

  TicketPriceCalculator getPriceCalculator(TicketType ticketType) {
    return calculators
        .stream()
        .filter(ticketPriceCalculator -> ticketPriceCalculator.isApplicable(ticketType))
        .findFirst().orElseThrow(() -> new TicketPriceCalculatorNotFound(ticketType.name()));
  }
}
