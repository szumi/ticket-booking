package pl.touk.cinema.reservation.domain;

interface ReservationRepository {

  Reservation save(Reservation reservation);
}
