package pl.touk.cinema.reservation.domain;

import lombok.extern.slf4j.Slf4j;
import pl.touk.cinema.screening.dto.DetailsScreeningDto;
import pl.touk.cinema.screening.dto.SeatDto;
import pl.touk.cinema.screening.dto.SeatIdDto;
import pl.touk.cinema.screening.exception.SeatNotFoundException;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
class ReservationValidator {

  private final Pattern NAME_AND_SURNAME_PATTERN =
    Pattern.compile("^\\p{Lu}\\p{Ll}{2,}[\\s]\\p{Lu}\\p{Ll}{2,}(-\\p{Lu}\\p{Ll}{2,})?");

  List<String> validate(Reservation reservation, DetailsScreeningDto screening) {
    List<String> errors = new ArrayList<>();

    // reservation applies to at least one seat.
    if (reservation.getSeats().size() < 1) {
      errors.add("Reservation should be applied to at least one seat");
    }

    if (reservation.getSeats().size() != reservation.getTotalTickets()) {
      errors.add("Number of tickets and reserved seats is not consistent");
    }

    if (isReservationPeriodExpired(reservation, screening)) {
      errors.add("Cannot create reservation. Seats can be booked at latest 15 minutes before the screening begins.");
    }

    if (isAnySeatAlreadyReserved(reservation.getSeats(), screening.getSeats())) {
      errors.add("One of requested seat is not longer available.");
    }

    if (!isCustomerNameCorrect(reservation.getCustomer())) {
      errors.add("Invalid format of customer name");
    }

    return errors;
  }

  private boolean isAnySeatAlreadyReserved(Set<SeatId> reservationSeats, List<SeatDto> screeningSeats) {
    return reservationSeats.stream()
      .anyMatch(reservationSeatId -> {
        SeatDto screeningSeat = getReservationSeatDetails(screeningSeats, reservationSeatId);
        return !screeningSeat.isAvailable();
      });
  }

  /**
   * Seats can be booked at latest 15 minutes before the screening begins.
   * */
  private static boolean isReservationPeriodExpired(Reservation reservation, DetailsScreeningDto screening) {
    return reservation.getCreateTime().isAfter(screening.getScreeningTime()) ||
      Duration.between(reservation.getCreateTime(), screening.getScreeningTime()).toMinutes() <= 15;
  }

  private SeatDto getReservationSeatDetails(List<SeatDto> screeningSeats, SeatId reservationSeatId) {
    return screeningSeats.stream()
      .filter(screeningSeat -> screeningSeat.getId().equals(new SeatIdDto(reservationSeatId.getId().toString())))
      .findFirst()
      .orElseThrow(() -> new SeatNotFoundException(new SeatIdDto(reservationSeatId.getId().toString())));
  }

  private boolean isCustomerNameCorrect(Customer customer) {
    Matcher matcher = NAME_AND_SURNAME_PATTERN.matcher(customer.getFullName());

    return matcher.matches();
  }
}
