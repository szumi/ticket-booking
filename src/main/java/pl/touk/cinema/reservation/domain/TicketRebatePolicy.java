package pl.touk.cinema.reservation.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.temporal.ValueRange;
import java.util.Currency;

import static java.time.DayOfWeek.FRIDAY;
import static java.time.DayOfWeek.SUNDAY;

interface TicketRebatePolicy {

  Amount apply(Amount ticketsBaseAmount, Quantity quantity);

  Integer order();

  boolean isApplicable(TicketRebateFactor rebateFactor);

  @RequiredArgsConstructor
  class WeekendRebatePolicy implements TicketRebatePolicy {

    private final Currency currency;

    @Override
    public Amount apply(Amount ticketsBaseAmount, Quantity quantity) {
      return new Amount(ticketsBaseAmount.getAmount().add(new BigDecimal(4 * quantity.getQuantity())), currency);
    }

    @Override
    public Integer order() {
      return 0;
    }

    @Override
    public boolean isApplicable(TicketRebateFactor rebateFactor) {
      ValueRange weekendRange = ValueRange.of(5, 7);
      DayOfWeek reservationDayOfWeek = rebateFactor.getScreeningTime().getDayOfWeek();

      if (!weekendRange.isValidValue(reservationDayOfWeek.getValue())) {
        return false;
      }

      if (FRIDAY == reservationDayOfWeek) {
        return rebateFactor.getScreeningTime().getHour() >= 14;
      } else if (SUNDAY == reservationDayOfWeek) {
        return rebateFactor.getScreeningTime().getHour() <= 11;
      }

      return true;
    }
  }

  @RequiredArgsConstructor
  class VoucherRebatePolicy implements TicketRebatePolicy {

    private final Currency currency;

    @Override
    public Amount apply(Amount ticketsBaseAmount, Quantity quantity) {
      return new Amount(ticketsBaseAmount.getAmount().multiply(new BigDecimal(0.5)), currency);
    }

    @Override
    public Integer order() {
      return Integer.MAX_VALUE;
    }

    @Override
    public boolean isApplicable(TicketRebateFactor rebateFactor) {
      return !StringUtils.isEmpty(rebateFactor.getRebateCode()) && "PROMO".equals(rebateFactor.getRebateCode());
    }
  }
}
