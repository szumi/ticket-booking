package pl.touk.cinema.reservation.domain;

enum TicketType {

  ADULT,
  STUDENT,
  CHILD
}
