package pl.touk.cinema.reservation.domain;

import lombok.RequiredArgsConstructor;
import pl.touk.cinema.reservation.dto.CreateReservationDto;
import pl.touk.cinema.reservation.dto.ReservationDto;
import pl.touk.cinema.reservation.exception.ReservationValidationException;
import pl.touk.cinema.screening.domain.ScreeningService;
import pl.touk.cinema.screening.dto.DetailsScreeningDto;

import java.util.List;

@RequiredArgsConstructor
public class ReservationService {

  private final ReservationCreator reservationCreator;
  private final ScreeningService screeningService;
  private final ReservationRepository reservationRepository;
  private final ReservationValidator validator;

  public synchronized ReservationDto createReservation(CreateReservationDto dto) {
    Reservation reservation = reservationCreator.from(dto);
    DetailsScreeningDto screening = screeningService.get(dto.getScreeningId());

    List<String> errors = validator.validate(reservation, screening);
    if (!errors.isEmpty()) {
      throw new ReservationValidationException(errors);
    }

    screeningService.reserveSeats(dto.getScreeningId(), dto.getSeatIds());

    return reservationRepository.save(reservation).dto();
  }
}
