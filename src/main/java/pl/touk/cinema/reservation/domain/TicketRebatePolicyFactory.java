package pl.touk.cinema.reservation.domain;

import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
class TicketRebatePolicyFactory {

  private final Set<TicketRebatePolicy> rebatePolicies;

  List<TicketRebatePolicy> getRebatePolicies(TicketRebateFactor ticketRebateFactor) {
    return rebatePolicies.stream()
      .filter(ticketRebatePolicy -> ticketRebatePolicy.isApplicable(ticketRebateFactor))
      .sorted(Comparator.comparing(TicketRebatePolicy::order))
      .collect(Collectors.toList());
  }
}
