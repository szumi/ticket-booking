package pl.touk.cinema.reservation.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
class Quantity {

  private Integer quantity;
}
