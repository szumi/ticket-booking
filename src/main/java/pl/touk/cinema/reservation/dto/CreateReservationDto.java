package pl.touk.cinema.reservation.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.touk.cinema.screening.dto.CustomerDto;
import pl.touk.cinema.screening.dto.ScreeningIdDto;
import pl.touk.cinema.screening.dto.SeatIdDto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Set;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateReservationDto {

  @NotNull
  private ScreeningIdDto screeningId;

  @NotNull
  private CustomerDto customer;

  @NotEmpty
  private Set<SeatIdDto> seatIds;

  @NotEmpty
  private Map<TicketTypeDto, QuantityDto> tickets;

  @NotNull
  private String rebateCode;
}
