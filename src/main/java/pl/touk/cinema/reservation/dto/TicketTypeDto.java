package pl.touk.cinema.reservation.dto;

public enum  TicketTypeDto {

  ADULT,
  STUDENT,
  CHILD
}
