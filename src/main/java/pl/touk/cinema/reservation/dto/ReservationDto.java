package pl.touk.cinema.reservation.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import pl.touk.cinema.screening.dto.AmountDto;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
public class ReservationDto {

  private ReservationIdDto id;
  private AmountDto amount;
  private LocalDateTime expirationTime;
}
