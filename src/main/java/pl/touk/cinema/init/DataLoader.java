package pl.touk.cinema.init;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.touk.cinema.screening.domain.ScreeningService;
import pl.touk.cinema.screening.dto.CreateScreeningDto;
import pl.touk.cinema.screening.dto.MovieDto;
import pl.touk.cinema.screening.dto.RoomDto;

import static java.time.LocalDateTime.of;

@Slf4j
@Profile("!test")
@Component
@RequiredArgsConstructor
class DataLoader implements ApplicationListener<ApplicationReadyEvent> {

  private final ScreeningService screeningService;

  @Override
  public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
    log.info("Loading sample data...");

    RoomDto roomA = new RoomDto("A", 20, 5);
    CreateScreeningDto filmA = CreateScreeningDto.builder()
        .movie(new MovieDto("FilmA"))
        .room(roomA)
        .screeningTime(of(2019, 8, 16, 19, 30))
        .build();
    screeningService.add(filmA);
    CreateScreeningDto filmA2 = CreateScreeningDto.builder()
        .movie(new MovieDto("FilmA2"))
        .room(roomA)
        .screeningTime(of(2019, 8, 16, 21, 30))
        .build();
    screeningService.add(filmA2);

    RoomDto roomB = new RoomDto("B", 11, 10);
    CreateScreeningDto filmB = CreateScreeningDto.builder()
        .movie(new MovieDto("FilmB"))
        .room(roomB)
        .screeningTime(of(2019, 8, 16, 19, 45))
        .build();
    screeningService.add(filmB);
    CreateScreeningDto filmB2 = CreateScreeningDto.builder()
        .movie(new MovieDto("FilmB2"))
        .room(roomB)
        .screeningTime(of(2019, 8, 16, 21, 45))
        .build();
    screeningService.add(filmB2);

    RoomDto roomC = new RoomDto("C", 5, 5);
    CreateScreeningDto filmC = CreateScreeningDto.builder()
        .movie(new MovieDto("FilmC"))
        .room(roomC)
        .screeningTime(of(2019, 8, 15, 19, 45))
        .build();
    screeningService.add(filmC);
    CreateScreeningDto filmC2 = CreateScreeningDto.builder()
        .movie(new MovieDto("FilmC2"))
        .room(roomC)
        .screeningTime(of(2019, 8, 17, 19, 45))
        .build();
    screeningService.add(filmC2);

    log.info("Loading sample data completed.");
  }
}
