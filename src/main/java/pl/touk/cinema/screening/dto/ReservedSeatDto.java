package pl.touk.cinema.screening.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class ReservedSeatDto implements SeatDto {

  private SeatIdDto id;
  private Integer rowNumber;
  private Integer seatNumber;

  @Override
  @JsonIgnore
  public Boolean isAvailable() {
    return false;
  }
}
