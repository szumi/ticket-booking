package pl.touk.cinema.screening.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Builder
@ToString
@AllArgsConstructor
public class SimpleScreeningDto {

  private ScreeningIdDto id;
  private LocalDateTime screeningTime;
  private MovieDto movie;
}
