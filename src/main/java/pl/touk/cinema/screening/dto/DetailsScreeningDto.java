package pl.touk.cinema.screening.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class DetailsScreeningDto {

  private ScreeningIdDto id;
  private LocalDateTime screeningTime;
  private List<SeatDto> seats;
}
