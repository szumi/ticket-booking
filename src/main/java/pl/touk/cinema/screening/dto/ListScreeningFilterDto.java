package pl.touk.cinema.screening.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@ToString
public class ListScreeningFilterDto {

  @NotNull
  private LocalDateTime from;

  @NotNull
  private LocalDateTime to;
}
