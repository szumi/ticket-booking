package pl.touk.cinema.screening.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = "name")
public class RoomDto {

  private String name;
  private Integer rowsNumber;
  private Integer rowSeatsNumber;
}
