package pl.touk.cinema.screening.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Currency;

@Getter
@AllArgsConstructor
public class AmountDto {

  private BigDecimal amount;
  private Currency currency;
}
