package pl.touk.cinema.screening.dto;

public interface SeatDto {

  SeatIdDto getId();
  Integer getRowNumber();
  Integer getSeatNumber();
  Boolean isAvailable();
}
