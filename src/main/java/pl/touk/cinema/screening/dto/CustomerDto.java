package pl.touk.cinema.screening.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class CustomerDto {

  @NotNull
  private String name;

  @NotNull
  private String surname;
}
