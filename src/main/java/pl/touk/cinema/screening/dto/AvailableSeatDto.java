package pl.touk.cinema.screening.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AvailableSeatDto implements SeatDto {

  private SeatIdDto id;
  private Integer rowNumber;
  private Integer seatNumber;

  @Override
  @JsonIgnore
  public Boolean isAvailable() {
    return true;
  }
}
