package pl.touk.cinema.screening.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
public class CreateScreeningDto {

  private MovieDto movie;
  private RoomDto room;
  private LocalDateTime screeningTime;
}
