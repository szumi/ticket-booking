package pl.touk.cinema.screening.infrastructure.adapters.web;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.touk.cinema.screening.domain.ScreeningService;
import pl.touk.cinema.screening.dto.ChooseScreeningDto;
import pl.touk.cinema.screening.dto.ListScreeningFilterDto;
import pl.touk.cinema.screening.dto.ScreeningIdDto;
import pl.touk.cinema.screening.dto.SimpleScreeningDto;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@RestController
@RequestMapping("/screenings")
@RequiredArgsConstructor
class ScreeningController {

  private final ScreeningService screeningService;

  @GetMapping
  List<SimpleScreeningDto> list(@RequestParam("from") @DateTimeFormat(iso = DATE_TIME)  LocalDateTime from,
      @DateTimeFormat(iso = DATE_TIME) @RequestParam("to") LocalDateTime to) {
    return screeningService.getScreeningsBy(new ListScreeningFilterDto(from, to));
  }

  @GetMapping("/{screeningId}")
  ChooseScreeningDto read(@PathVariable("screeningId") String screeningId) {
    return screeningService.choose(new ScreeningIdDto(screeningId));
  }
}
