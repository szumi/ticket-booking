package pl.touk.cinema.screening.domain;

import lombok.ToString;

@ToString
enum SeatStatus {

  AVAILABLE,
  RESERVED,
}
