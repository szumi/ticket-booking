package pl.touk.cinema.screening.domain;

import lombok.AllArgsConstructor;
import pl.touk.cinema.screening.dto.ChooseScreeningDto;
import pl.touk.cinema.screening.dto.CreateScreeningDto;
import pl.touk.cinema.screening.dto.DetailsScreeningDto;
import pl.touk.cinema.screening.dto.ListScreeningFilterDto;
import pl.touk.cinema.screening.dto.ScreeningDto;
import pl.touk.cinema.screening.dto.ScreeningIdDto;
import pl.touk.cinema.screening.dto.SeatIdDto;
import pl.touk.cinema.screening.dto.SimpleScreeningDto;
import pl.touk.cinema.screening.exception.InvalidDateRangeException;
import pl.touk.cinema.screening.exception.ScreeningNotFoundException;
import pl.touk.cinema.screening.exception.ScreeningSeatsValidationException;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.UUID.fromString;
import static java.util.stream.Collectors.toList;

@AllArgsConstructor
public class ScreeningService {

  private final ScreeningRepository screeningRepository;
  private final ScreeningCreator screeningCreator;
  private final ScreeningSeatsValidator screeningSeatsValidator;

  //TODO null value handling
  //TODO sorting
  public List<SimpleScreeningDto> getScreeningsBy(ListScreeningFilterDto filterDto) {
    if (filterDto.getFrom().isAfter(filterDto.getTo())) {
      throw new InvalidDateRangeException();
    }

    return screeningRepository.findBy(filterDto.getFrom(), filterDto.getTo())
        .stream()
        .map(Screening::simpleDto)
        .collect(toList());
  }

  public DetailsScreeningDto get(ScreeningIdDto id) {
    return screeningRepository.read(new ScreeningId(fromString(id.getId())))
      .map(Screening::detailsDto)
      .orElseThrow(() -> new ScreeningNotFoundException(id));
  }

  public ScreeningDto add(CreateScreeningDto dto) {
    Screening screening = screeningCreator.from(dto);
    return screeningRepository.save(screening).dto();
  }

  public ChooseScreeningDto choose(ScreeningIdDto id) {
    return screeningRepository.read(new ScreeningId(fromString(id.getId())))
        .map(Screening::chooseDto)
        .orElseThrow(() -> new ScreeningNotFoundException(id));
  }

  public void reserveSeats(ScreeningIdDto screeningId, Set<SeatIdDto> seatIds) {
    Screening screening = screeningRepository.read(new ScreeningId(fromString(screeningId.getId())))
      .orElseThrow(() -> new ScreeningNotFoundException(screeningId));

    List<String> errors = screeningSeatsValidator.validate(screening.getSeats()
      .stream()
      .map(Seat::dto)
      .collect(toList()), seatIds);

    if (!errors.isEmpty()) {
      throw new ScreeningSeatsValidationException(errors);
    }

    screening.reserveSeats(seatIds.stream().map(seatIdDto -> new SeatId(fromString(seatIdDto.getId())))
      .collect(Collectors.toSet()));
  }
}
