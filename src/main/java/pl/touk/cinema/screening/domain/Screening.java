package pl.touk.cinema.screening.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import pl.touk.cinema.screening.dto.AvailableSeatDto;
import pl.touk.cinema.screening.dto.ChooseScreeningDto;
import pl.touk.cinema.screening.dto.DetailsScreeningDto;
import pl.touk.cinema.screening.dto.ScreeningDto;
import pl.touk.cinema.screening.dto.ScreeningIdDto;
import pl.touk.cinema.screening.dto.SeatDto;
import pl.touk.cinema.screening.dto.SeatIdDto;
import pl.touk.cinema.screening.dto.SimpleScreeningDto;
import pl.touk.cinema.screening.exception.SeatNotFoundException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
class Screening {

  private ScreeningId id;
  private Movie movie;
  private Room room;
  private List<Seat> seats;
  private LocalDateTime screeningTime;

  void reserveSeats(Set<SeatId> seatIds) {
    seatIds.stream()
        .map(this::getSeat)
        .forEach(Seat::makeReservation);
  }

  private Seat getSeat(SeatId id) {
    return seats.stream().filter(ss -> ss.getId().equals(id)).findFirst()
        .orElseThrow(() -> new SeatNotFoundException(new SeatIdDto(id.getId().toString())));
  }

  SimpleScreeningDto simpleDto() {
    return SimpleScreeningDto.builder()
        .id(new ScreeningIdDto(id.getId().toString()))
        .movie(movie.dto())
        .screeningTime(screeningTime)
        .build();
  }

  ScreeningDto dto() {
    return ScreeningDto.builder()
        .id(new ScreeningIdDto(id.getId().toString()))
        .movie(movie.dto())
        .room(room.dto())
        .screeningTime(screeningTime)
        .build();
  }

  DetailsScreeningDto detailsDto() {
    return DetailsScreeningDto.builder()
      .id(new ScreeningIdDto(id.getId().toString()))
      .seats(getAllSeats())
      .screeningTime(screeningTime)
      .build();
  }

  private List<SeatDto> getAllSeats() {
    return seats.stream()
      .map(Seat::dto)
      .collect(Collectors.toList());
  }

  ChooseScreeningDto chooseDto() {
    return ChooseScreeningDto.builder()
        .id(new ScreeningIdDto(id.getId().toString()))
        .movie(movie.dto())
        .availableSeats(getAvailableSeats())
        .room(room.dto())
        .screeningTime(screeningTime)
        .build();
  }

  private List<AvailableSeatDto> getAvailableSeats() {
    return seats.stream()
        .filter(seat -> seat.getSeatStatus() == SeatStatus.AVAILABLE)
        .map(Seat::availableSeatDto)
        .collect(Collectors.toList());
  }
}

