package pl.touk.cinema.screening.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.touk.cinema.screening.dto.RoomDto;

@Getter
@AllArgsConstructor
class Room {

  private String name;
  private Integer rowsNumber;
  private Integer rowSeatsNumber;

  RoomDto dto() {
    return new RoomDto(name, rowsNumber, rowSeatsNumber);
  }
}
