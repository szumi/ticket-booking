package pl.touk.cinema.screening.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import pl.touk.cinema.screening.dto.ScreeningIdDto;

import java.util.UUID;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
class ScreeningId {

  private final UUID id;

  ScreeningIdDto dto() {
    return new ScreeningIdDto(id.toString());
  }
}
