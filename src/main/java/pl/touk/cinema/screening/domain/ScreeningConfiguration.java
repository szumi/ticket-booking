package pl.touk.cinema.screening.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ScreeningConfiguration {

  @Bean
  public ScreeningService screeningService() {
    return new ScreeningService(screeningRepository(), new ScreeningCreator(), new ScreeningSeatsValidator());
  }

  @Bean
  ScreeningRepository screeningRepository() {
    return new InMemoryScreeningRepository();
  }
}
