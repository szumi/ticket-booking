package pl.touk.cinema.screening.domain;

import pl.touk.cinema.screening.dto.CreateScreeningDto;

import java.util.ArrayList;
import java.util.List;

import static java.util.UUID.randomUUID;
import static pl.touk.cinema.screening.domain.SeatStatus.AVAILABLE;

class ScreeningCreator {

  Screening from(CreateScreeningDto dto) {
    ScreeningId id = new ScreeningId(randomUUID());
    Movie movie = new Movie(dto.getMovie().getTitle());
    Room room = new Room(dto.getRoom().getName(), dto.getRoom().getRowsNumber(), dto.getRoom().getRowSeatsNumber());

    List<Seat> seats = new ArrayList<>();
    for (int rowIndex = 0; rowIndex < dto.getRoom().getRowsNumber(); rowIndex++) {
      for (int seatIndex = 0; seatIndex < dto.getRoom().getRowSeatsNumber(); seatIndex++) {
        Seat seat = new Seat(new SeatId(randomUUID()), id, seatIndex, rowIndex, AVAILABLE);
        seats.add(seat);
      }
    }

    return new Screening(id, movie, room, seats, dto.getScreeningTime());
  }
}
