package pl.touk.cinema.screening.domain;

import lombok.extern.slf4j.Slf4j;
import pl.touk.cinema.screening.dto.SeatDto;
import pl.touk.cinema.screening.dto.SeatIdDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;

@Slf4j
class ScreeningSeatsValidator {

  List<String> validate(List<SeatDto> roomSeats, Set<SeatIdDto> seatIdsToReserve) {
    List<String> errors = new ArrayList<>();

    if (isSingleSeatEmptyBetweenTwoReservedSeats(seatIdsToReserve, roomSeats)) {
      errors.add("There cannot be a single place left over in a row between two already reserved places.");
    }

    return errors;
  }

  /**
   * There cannot be a single place left over in a row between two already reserved places.
   */
  private boolean isSingleSeatEmptyBetweenTwoReservedSeats(Set<SeatIdDto> reservationSeats, List<SeatDto> roomSeats) {

    // Creates helper matrix that represent room state after applying reservation
    Vector<Vector<Short>> roomMatrix = new Vector<>();
    roomSeats.forEach(seat -> {
      if (roomMatrix.size() != seat.getRowNumber() + 1) {
        roomMatrix.add(seat.getRowNumber(), new Vector<>());
      }

      Optional<SeatIdDto> reservationSeat = findReservationSeatDetails(reservationSeats, seat);

      roomMatrix.get(seat.getRowNumber())
        .add((short) (!seat.isAvailable() || reservationSeat.isPresent() ? 1 : 0));
    });

    roomMatrix.forEach(shorts -> log.info("{}", shorts));

    // Validation
    return roomMatrix.stream()
      .anyMatch(row -> {
        for (int seatNumber = 0; seatNumber < row.size(); seatNumber++) {

          if (row.get(seatNumber) == (short) 1) {
            continue;
          }

          // outermost seats can be empty (?)
          if (isOutermostSeat(row.size() - 1, seatNumber)) {
            continue;
          }

          return row.get(seatNumber - 1) == (short) 1 && row.get(seatNumber + 1) == (short) 1;
        }

        return false;
      });
  }

  private Optional<SeatIdDto> findReservationSeatDetails(Set<SeatIdDto> reservationSeats, SeatDto seat) {
    return reservationSeats.stream()
      .filter(seatId -> seatId.equals(seat.getId()))
      .findAny();
  }

  private static boolean isOutermostSeat(int seatsInRowNumber, int seatNumber) {
    return seatNumber == 0 || seatNumber == seatsInRowNumber;
  }
}
