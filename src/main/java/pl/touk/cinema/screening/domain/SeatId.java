package pl.touk.cinema.screening.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import pl.touk.cinema.screening.dto.SeatIdDto;

import java.util.UUID;

@ToString
@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
class SeatId {

  private final UUID id;

  SeatIdDto dto() {
    return new SeatIdDto(id.toString());
  }
}
