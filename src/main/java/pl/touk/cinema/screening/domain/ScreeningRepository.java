package pl.touk.cinema.screening.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

interface ScreeningRepository {

  Screening save(Screening screening);

  List<Screening> findBy(LocalDateTime from, LocalDateTime to);

  Optional<Screening> read(ScreeningId id);
}
