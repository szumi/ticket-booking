package pl.touk.cinema.screening.domain;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toList;

class InMemoryScreeningRepository implements ScreeningRepository {

  private final ConcurrentHashMap<ScreeningId, Screening> screeningMap = new ConcurrentHashMap<>();

  @Override
  public Screening save(Screening screening) {
    screeningMap.put(screening.getId(), screening);
    return screeningMap.get(screening.getId());
  }

  @Override
  public List<Screening> findBy(LocalDateTime from, LocalDateTime to) {
    //TODO externalize sort parameter to controller parameter
    return screeningMap.values()
        .stream()
        .filter(screening -> isInRange(screening.getScreeningTime(), from, to))
        .sorted(Comparator.comparing(Screening::getScreeningTime).thenComparing(screening -> screening.getMovie().getTitle()))
        .collect(toList());
  }

  private boolean isInRange(LocalDateTime screeningTime, LocalDateTime start, LocalDateTime end) {
    return (screeningTime.isAfter(start) || screeningTime.isEqual(start)) && (screeningTime.isBefore(end) || screeningTime.isEqual(end));
  }

  @Override
  public Optional<Screening> read(ScreeningId id) {
    return Optional.ofNullable(screeningMap.get(id));
  }
}
