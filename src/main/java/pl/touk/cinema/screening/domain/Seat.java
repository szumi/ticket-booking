package pl.touk.cinema.screening.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import pl.touk.cinema.screening.dto.AvailableSeatDto;
import pl.touk.cinema.screening.dto.ReservedSeatDto;
import pl.touk.cinema.screening.dto.SeatDto;
import pl.touk.cinema.screening.exception.SeatAlreadyReservedException;

@ToString
@Getter
@AllArgsConstructor
class Seat {

  private SeatId id;
  private ScreeningId screening;
  private Integer seatNumber;
  private Integer rowNumber;
  private SeatStatus seatStatus;

  void makeReservation() {
    if (seatStatus != SeatStatus.AVAILABLE) {
      throw new SeatAlreadyReservedException((id.dto()));
    }

    this.seatStatus = SeatStatus.RESERVED;
  }

  AvailableSeatDto availableSeatDto() {
    assert  seatStatus == SeatStatus.AVAILABLE;

    return new AvailableSeatDto(id.dto(), rowNumber, seatNumber);
  }

  SeatDto dto() {
    return seatStatus == SeatStatus.AVAILABLE ? new AvailableSeatDto(id.dto(), rowNumber, seatNumber) :
      new ReservedSeatDto(id.dto(), rowNumber, seatNumber);
  }
}
