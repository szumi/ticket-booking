package pl.touk.cinema.screening.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.touk.cinema.screening.dto.MovieDto;

@Getter
@AllArgsConstructor
class Movie {

  private String title;

  MovieDto dto() {
    return new MovieDto(title);
  }
}
