package pl.touk.cinema.screening.exception;

import pl.touk.cinema.screening.dto.ScreeningIdDto;

public class ScreeningNotFoundException extends RuntimeException {

  public ScreeningNotFoundException(ScreeningIdDto id) {
    super("Screening not found for given identifier: " + id.getId());
  }
}
