package pl.touk.cinema.screening.exception;

import lombok.Getter;

import java.util.List;

@Getter
public class ScreeningSeatsValidationException extends RuntimeException {

  private List<String> errors;

  public ScreeningSeatsValidationException(List<String> errors) {
    this.errors = errors;
  }
}
