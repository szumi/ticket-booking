package pl.touk.cinema.screening.exception;

import pl.touk.cinema.screening.dto.SeatIdDto;

public class SeatNotFoundException extends RuntimeException {

  public SeatNotFoundException(SeatIdDto id) {
    super("Seat not found with given identifier " + id.getId());
  }
}
