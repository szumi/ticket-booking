package pl.touk.cinema.screening.exception;

import pl.touk.cinema.screening.dto.SeatIdDto;

public class SeatAlreadyReservedException extends RuntimeException {

  public SeatAlreadyReservedException(SeatIdDto id) {
    super("Seat with given id: " + id.getId() + " is already reserved.");
  }
}
