package pl.touk.cinema.screening.exception;

public class NotSupportedOperationException extends RuntimeException {

  public NotSupportedOperationException(String message) {
    super(message);
  }
}
