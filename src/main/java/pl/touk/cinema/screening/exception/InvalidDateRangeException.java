package pl.touk.cinema.screening.exception;

public class InvalidDateRangeException extends RuntimeException {

  public InvalidDateRangeException() {
    super("Invalid date range exception");
  }
}
